package com.gmartinez.campisland.springboot.backendchallenge.repository;

import com.gmartinez.campisland.springboot.backendchallenge.model.Campsite;
import org.springframework.data.repository.CrudRepository;

public interface CampsiteRepository extends CrudRepository <Campsite, Long> {

}
