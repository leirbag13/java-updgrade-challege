package com.gmartinez.campisland.springboot.backendchallenge.repository;

import com.gmartinez.campisland.springboot.backendchallenge.model.Reserve;
import org.springframework.data.repository.CrudRepository;

public interface ReserveRepository extends CrudRepository <Reserve, Long> {

}
