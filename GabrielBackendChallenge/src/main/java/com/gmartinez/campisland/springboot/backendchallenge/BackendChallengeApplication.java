package com.gmartinez.campisland.springboot.backendchallenge;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.gmartinez.campisland.springboot.backendchallenge.model.Campsite;
import com.gmartinez.campisland.springboot.backendchallenge.repository.CampsiteRepository;

@SpringBootApplication(scanBasePackages = {"com.gmartinez.campisland.springboot.backendchallenge"})
public class BackendChallengeApplication {

	public static void main(String[] args) {
		System.setProperty("spring.devtools.restart.enabled", "true");
		SpringApplication.run(BackendChallengeApplication.class, args);
	}
	
	@Bean	
	public CommandLineRunner demo(CampsiteRepository campsiteRepository) {
		return (args) -> {
			// Create the campsite
			Campsite campsite = new Campsite();
			campsite.setName("Volcano Island Raskita");
			campsiteRepository.save(campsite);
			
			Campsite campsite2 = new Campsite();
			campsite2.setName("Passover Islands");
			campsiteRepository.save(campsite2);
		};
	}
}
